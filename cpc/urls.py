from django.conf.urls import patterns, include, url

from almacen.viewsets import ProductoViewSet, UbicacionViewSet,Origen_productoViewSet,Causa_NCViewSet,TratamientoViewSet,CausalViewSet,Control_ProductoViewSet
from rest_framework.routers import DefaultRouter
router = DefaultRouter()

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

router.register(r'Producto', ProductoViewSet)
router.register(r'Ubicacio', UbicacionViewSet)
router.register(r'Origen_product', Origen_productoViewSet)
router.register(r'Causa_NC', Causa_NCViewSet)
router.register(r'Tratamiento', TratamientoViewSet)
router.register(r'Tratamiento', TratamientoViewSet)
router.register(r'Causal', CausalViewSet)
router.register(r'Control_Producto', Control_ProductoViewSet)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'cpc.views.home', name='home'),
    # url(r'^cpc/', include('cpc.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^', include(router.urls)),
     url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
     url(r'^admin/', include(admin.site.urls)),
)
