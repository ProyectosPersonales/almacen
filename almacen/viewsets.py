from .models import Producto,Ubicacion,Origen_producto,Causa_NC,Tratamiento,Causal,Control_Producto
from .serializers import ProductoSerializer, UbicacionSerializer,Origen_productoSerializer,Causa_NCSerializer,TratamientoSerializer,CausalSerializer,Control_ProductoSerializer
from rest_framework import viewsets

class ProductoViewSet(viewsets.ModelViewSet):
    serializer_class = ProductoSerializer
    queryset = Producto.objects.all()

class UbicacionViewSet(viewsets.ModelViewSet):

    serializer_class = UbicacionSerializer
    queryset = Ubicacion.objects.all()

class Origen_productoViewSet(viewsets.ModelViewSet):

    serializer_class = Origen_productoSerializer
    queryset = Origen_producto.objects.all()

class Causa_NCViewSet(viewsets.ModelViewSet):
    
    serializer_class = Causa_NCSerializer
    queryset = Causa_NC.objects.all()

class TratamientoViewSet(viewsets.ModelViewSet):

    serializer_class = TratamientoSerializer
    queryset = Tratamiento.objects.all()

class CausalViewSet(viewsets.ModelViewSet):

    serializer_class = CausalSerializer
    queryset = Causal.objects.all()

class Control_ProductoViewSet(viewsets.ModelViewSet):

    serializer_class = Control_ProductoSerializer
    queryset = Control_Producto.objects.all()