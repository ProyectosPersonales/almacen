from rest_framework import serializers
from .models import Producto,Ubicacion,Origen_producto,Causa_NC,Tratamiento,Causal,Control_Producto
 
class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ('id', 'codigo', 'nombre_producto',)
 
class UbicacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ubicacion
        fields = ('id', 'nombre_ubicacion',)

class Origen_productoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Origen_producto
        fields = ('id', 'origen',)

class Causa_NCSerializer(serializers.ModelSerializer):
    class Meta:
        model = Causa_NC
        fields = ('id', 'convencion','nombre_NC',)

class TratamientoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tratamiento
        fields = ('id', 'tipo',)

class CausalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Causal
        fields = ('id', 'nombre_causal',)

class Control_ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Control_Producto
        fields = ('id', 'Fecha','Producto','Origen_producto','Lote','Cantidad','Ubicacion','Causa_NC','Tratamiento','Causa','Creado_por')