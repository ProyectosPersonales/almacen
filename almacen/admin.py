from django.contrib import admin
from models import *
from actions import export_as_csv


class Control_ProductoAdmin(admin.ModelAdmin):
    list_display = ('Fecha', 'Producto', 'Origen_producto', 'Lote', 'Cantidad','Ubicacion','Causa_NC','Causa','Creado_por')
    list_filter = ('Fecha', 'Creado_por','Producto')
    search_fields = ( 'Producto__nombre_producto', 'Producto___codigo')
    #list_editable = ('producto', 'enlace', 'categoria')
    #list_display_links = ('es_popular',)
    actions = [export_as_csv]
    raw_id_fields = ('Producto',)


class Causa_NCAdmin(admin.ModelAdmin):
    list_display = ('convencion', 'nombre_NC')
    list_filter = ('convencion',)

class Producto_NCAdmin(admin.ModelAdmin):
    list_display = ('codigo', 'nombre_producto')
    list_filter = ('nombre_producto',)
    search_fields = ( 'nombre_producto', 'codigo')

class Origen_productoAdmin(admin.ModelAdmin):
    list_display = ('id', 'origen')
    list_filter = ('origen',)
    search_fields = ( 'origen',)


admin.site.register(Control_Producto,Control_ProductoAdmin)
admin.site.register(Producto,Producto_NCAdmin)
admin.site.register(Origen_producto, Origen_productoAdmin)
admin.site.register(Causa_NC,Causa_NCAdmin)
admin.site.register(Tratamiento)
admin.site.register(Causal)
admin.site.register(Ubicacion)