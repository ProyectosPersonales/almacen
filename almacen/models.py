from django.db import models
from django.contrib.auth.models import User

class Producto(models.Model):
    codigo = models.CharField(max_length = 50)
    nombre_producto = models.CharField(max_length = 140,verbose_name ='Nombre producto')
    class Meta:
         verbose_name = "Producto"

    def __unicode__(self):
       return "%s - %s" % (self.codigo,self.nombre_producto)

class Ubicacion(models.Model):
    nombre_ubicacion = models.CharField(max_length = 140,verbose_name ='Nombre Ubicacion')

    def __unicode__(self):
        return self.nombre_ubicacion

class Origen_producto(models.Model):
     origen      = models.CharField(max_length = 140)
     class Meta:
         verbose_name = "Origen de producto"
     
     def __unicode__(self):
        return self.origen


class Causa_NC(models.Model):
    convencion      = models.CharField(max_length = 50)
    nombre_NC       = models.CharField(max_length = 140,verbose_name ='No conformidad')
    class Meta:
         verbose_name = "Causa no conformidad"


    def __unicode__(self):
        return "%s - %s" % (self.convencion,self.nombre_NC)

class Tratamiento(models.Model):
      tipo      = models.CharField(max_length = 140)

      def __unicode__(self):
        return self.tipo

class Causal(models.Model):
    nombre_causal = models.CharField(max_length = 140,verbose_name ='Nombre causal')

    def __unicode__(self):
        return self.nombre_causal

class Control_Producto(models.Model):
     Fecha  =   models.DateTimeField(auto_now_add=True)
     Producto   =   models.ForeignKey(Producto)
     Origen_producto    = models.ForeignKey(Origen_producto,verbose_name='origen producto')
     Lote  =    models.PositiveIntegerField(default = 0)
     Cantidad   =   models.PositiveIntegerField(default = 0)
     Ubicacion      = models.ForeignKey(Ubicacion)
     Causa_NC    = models.ForeignKey(Causa_NC)
     Tratamiento    = models.ForeignKey(Tratamiento)
     Causa    = models.ForeignKey(Causal)
     Creado_por  =  models.ForeignKey(User,verbose_name='Creado por')
     class Meta:
         verbose_name = "CONTROL PRODUCTO"

     def __unicode__(self):
        return "%s - %s - %s " % (self.Fecha,self.Producto,self.Creado_por)


